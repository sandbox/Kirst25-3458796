<?php

namespace Drupal\style_fields\Element;

use Drupal\Core\Render\Element\Radios;

/**
 * Renders the bg_selector form element.
 *
 * @FormElement("bg_selector")
 */
class BgSelector extends Radios {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $info = parent::getInfo();
    $info['#theme_wrappers'] = ['color_field_swatch'];
    return $info;
  }

}
