<?php

namespace Drupal\style_fields\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsWidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'boolean_checkbox' widget.
 *
 * @FieldWidget(
 *   id = "color_options",
 *   label = @Translation("Color Options"),
 *   field_types = {
 *     "list_string",
 *   },
 *   multiple_values = TRUE
 * )
 */
class ColorOptions extends OptionsWidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'color' => [],
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    $values = $this->fieldDefinition->getFieldStorageDefinition()->getSetting('allowed_values');

    $elements['color'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Colour values'),
      '#description' => $this->t('The colour values for each option. Eg. #123123, rgba(0,0,0,0)'),
    ];

    foreach ($values as $key => $value) {
      $elements['color'][$key] = [
        '#type' => 'textfield',
        '#title' => $value,
        '#default_value' => $this->getSetting('color')[$key] ?? '',
      ];
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $options = $this->getOptions($items->getEntity());
    $selected = $this->getSelectedOptions($items);

    $color_settings = $this->getSetting('color');

    // If required and there is one single option, preselect it.
    if ($this->required && count($options) === 1) {
      reset($options);
      $selected = key($options);
    }

    $default_value = key($selected);
    if (!$this->multiple) {
      $default_value = $selected ? reset($selected) : NULL;
    }

    $field_name = $this->fieldDefinition->getName();
    $parents = array_merge($form['#parents'], [$field_name]);
    $first_parent = $form['#parents'] ? array_shift($parents) : NULL;

    $element += [
      '#type' => 'bg_selector',
      '#default_value' => $default_value ?: '_none',
      '#options' => $options,
      '#field_name' => $field_name,
      '#colors' => $color_settings,
      '#field_path' => $first_parent ? $first_parent . '[' . implode('][', $parents) . ']' : $field_name,
      '#id' => Html::getUniqueId('color--' . Html::getId($field_name)),
    ];

    // Most widgets need their internal structure preserved in submitted values.
    $element += ['#tree' => TRUE];

    $element['#attached']['library'][] = 'style_fields/swatch_field';
    $element['#attached']['drupalSettings']['color_swatch'] = $color_settings ?: $options;

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEmptyLabel() {
    if (!$this->required && !$this->multiple) {
      return $this->t('Default');
    }
  }

}
