<?php

namespace Drupal\style_fields\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsWidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Heading widget for the heading field type.
 *
 * @FieldWidget(
 *   id = "spacing_options",
 *   label = @Translation("Spacing options"),
 *   field_types = {
 *     "boolean",
 *     "entity_reference",
 *     "list_integer",
 *     "list_float",
 *     "list_string",
 *   },
 *   multiple_values = TRUE
 * )
 */
class SpacingOptions extends OptionsWidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'space_type' => 'margin-top',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['space_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Space type'),
      '#options' => [
        'margin-top' => $this->t('Margin top'),
        'padding-top' => $this->t('Padding top'),
        'margin-bottom' => $this->t('Margin bottom'),
        'padding-bottom' => $this->t('Padding bottom'),
      ],
      '#default_value' => $this->getSetting('space_type'),
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $space_type = $this->getSetting('space_type');
    $summary[] = $this->t('Spacing type: @type', ['@type' => $space_type]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['#attached']['library'][] = 'style_fields/spacing_field';
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $options = $this->getOptions($items->getEntity());
    $selected = $this->getSelectedOptions($items);
    $space_type = $this->getSetting('space_type');

    // If required and there is one single option, preselect it.
    if ($this->required && count($options) == 1) {
      reset($options);
      $selected = [key($options)];
    }

    if ($this->multiple) {
      $element += [
        '#type' => 'checkboxes',
        '#default_value' => $selected ?: ['_none'],
        '#options' => $options,
      ];
    }
    else {
      $element += [
        '#type' => 'radios',
        // Radio buttons need a scalar value. Take the first default value, or
        // default to NULL so that the form element is properly recognized as
        // not having a default value.
        '#default_value' => $selected ? reset($selected) : NULL,
        '#options' => $options,
      ];
    }

    $element['#attributes']['class'][] = 'spacing-selector';
    $element['#attributes']['class'][] = $space_type;

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEmptyLabel() {
    if (!$this->required && !$this->multiple) {
      return $this->t('None');
    }
  }

}
