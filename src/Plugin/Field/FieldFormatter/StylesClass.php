<?php

namespace Drupal\style_fields\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'StylesClass' formatter.
 *
 * @FieldFormatter(
 *   id = "styles_class",
 *   label = @Translation("Styles class"),
 *   field_types = {
 *     "string",
 *     "list_string"
 *   }
 * )
 */
class StylesClass extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $class_prefix = $this->getSetting('class_prefix');
    $summary = [];
    $summary[] = $this->t('Applies the value as a class to the entity');
    if ($class_prefix) {
      $summary[] = $this->t('Class prefix: @prefix', ['@prefix' => $class_prefix]);
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'class_prefix' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['class_prefix'] = [
      '#title' => $this->t('Class prefix'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('class_prefix'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    $class_prefix = $this->getSetting('class_prefix');

    foreach ($items as $delta => $item) {
      $value = $class_prefix ? $class_prefix . $item->value : $item->value;
      $element[$delta] = [
        '#value' => Html::cleanCssIdentifier($value),
      ];
    }

    return $element;
  }

}
