<?php

namespace Drupal\style_fields\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'BgColorStyle' formatter.
 *
 * @FieldFormatter(
 *   id = "bg_color_style",
 *   label = @Translation("Background color style"),
 *   field_types = {
 *     "list_string"
 *   }
 * )
 */
class BgColorStyle extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Applies style="background-color: value" to the entity.');
    $summary[] = $this->t('Format: @format', ['@format' => $this->getSetting('format')]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'format' => 'variable',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['format'] = [
      '#title' => $this->t('Format'),
      '#type' => 'select',
      '#options' => [
        'variable' => $this->t('CSS variable'),
        'hex' => $this->t('Hex code'),
      ],
      '#default_value' => $this->getSetting('format'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    $format = $this->getSetting('format');

    foreach ($items as $delta => $item) {
      $value = $this->formattedValue($item->value, $format);
      $element[$delta]['#style'] = $value ? 'background-color: ' . $value . ';' : '';
    }

    return $element;
  }

  /**
   * Formatted value.
   *
   * @param string $item_value
   *   The field value.
   * @param string $format
   *   The format value.
   *
   * @return string|null
   *   The new string value or null.
   */
  private function formattedValue(string $item_value, string $format) {
    if (!$format) {
      return $item_value;
    }

    if ($format === 'variable') {
      $ccs_var = str_starts_with($item_value, '--') ? $item_value : '--sf-bgcolor-' . $item_value;
      return 'var(' . $ccs_var . ')';
    }

    if ($format === 'hex') {
      return ctype_xdigit($item_value) ? '#' . $item_value : NULL;
    }
  }

}
