## INTRODUCTION

The Style fields module adds field widgets for selecting color or spacing
options.

It also includes field formatters to add the field as a css class or inline
style to the entity.

![Example of style widgets](assets/example.png "Example of style widgets")

## CONFIGURATION
- Add a list (text) field - Note: The key will be used as the class.
- Configure the form display to use Spacing or Color widgets.
- [Optional] Configure the manage display to use class of style formatters.

---

## EXAMPLES

### BG color lists:
List (text) field with:
```
--c-primary|#cbd7a6
--c-red|#e66363
--c-blue|#bdc7e1
```

Set display as "Color options" and define the colors.

---

### Spacing fields:
Separate margin-top, margin-bottom, padding-top, padding-bottom list (text)
fields with:
```
mb--s|S
mb--m|M
mb--l|L
```
But also as mt, pb, pt.

Set display as "Spacing options" and select the correct type.
