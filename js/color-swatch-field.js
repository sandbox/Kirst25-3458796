/**
 * @file
 * JS file for colorSwatchField.
 */
((Drupal, drupalSettings, once) => {
  /**
   * Behavior for colorSwatchField.
   */
  Drupal.behaviors.colorSwatchField = {
    /**
     * Attach behaviour.
     */
    attach() {
      if (!once('color-swatch', 'html').length) return;

      const colorSwatch = drupalSettings.color_swatch;
      if (!colorSwatch) return;

      let string = '';
      Object.keys(colorSwatch).forEach((key) => {
        if (key !== '_none') return;
        if (key.startsWith('--')) {
          string += `${key}: ${colorSwatch[key]};`;
        }
      });

      if (string !== '') {
        const style = document.createElement('style');
        style.innerHTML = `
            :root {\n\
              ${string}
            }\n\
          `;

        document.body.append(style);
      }
    },
  };
})(Drupal, drupalSettings, once);
